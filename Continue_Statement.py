###! /bin/python
###============================================================================
### Script name: Continue_Statement.py
### Script type: Python script
### Description: To Check Sum of Positive Numbers in Python by using Continue
###              Statement
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
nums = [1, 2, -3, 4, -5, 6]
sum_positives = 0 
###============================================================================
### Main Functions
###============================================================================
for num in nums:
    if num < 0:
        continue
    sum_positives += num
print(f'Sum of Positive Numbers: {sum_positives}') 
###============================================================================
### End
###============================================================================
