###! /bin/python
###============================================================================
### Script name: 0_to_9_number_print_in_for_loop.py
### Script type: Python script
### Description: Printing upto 0 to 9 in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
for i in range(10):
    for j in range(5):
        print(i,end=" ")
    print()
###============================================================================
### End
###============================================================================
