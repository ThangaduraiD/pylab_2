###! /bin/python
###============================================================================
### Script name: College_Mark_list_Preparation_Project.py
### Script type: Python script
### Description: College Marklist Preparation project by Json file in Python
###
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 2.0                                                Date: 2022-04-04
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Modules
###============================================================================
import json
###============================================================================
### Local Variables
###============================================================================
Mark_list = {
    "Students":[
        {
        "Name":"Siva",
        "DateofBirth":"15-04-1998",
        "Placeofbirth":"Ariyalur",
        "Roll Number":"0001"
        },
        {
        "Name":"Surya",
        "DateofBirth":"15-04-1998",
        "Placeofbirth":"Ariyalur",
        "Roll Number":"0002"   
        }
        ],
    "Semester-I": [
        {
            "Roll Number":"0001",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"   
        },
        {
            "Roll Number":"0002",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"     
        }
    ],
    "Semester-II": [
        {
            "Roll Number":"0001",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"   
        },
        {
            "Roll Number":"0002",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"     
        }
    ],
    "Semester-III": [
        {
            "Roll Number":"0001",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"   
        },
        {
            "Roll Number":"0002",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"     
        }
    ],
    "Semester-IV": [
        {
            "Roll Number":"0001",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"   
        },
        {
            "Roll Number":"0002",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"     
        }
    ],
    "Semester-V": [
        {
            "Roll Number":"0001",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"   
        },
        {
            "Roll Number":"0002",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"     
        }
    ],
    "Semester-VI": [
        {
            "Roll Number":"0001",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"   
        },
        {
            "Roll Number":"0002",
            "Physics":"100",
            "Chemistrty":"100",
            "Maths":"100"     
        }
    ],

    }
jsonString = json.dumps(Mark_list)
jsonFile = open("Mark_list.json", "w")
jsonFile.write(jsonString)
jsonFile.close()