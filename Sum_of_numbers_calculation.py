###! /bin/python
###============================================================================
### Script name: Sum_of_Numbers_Calculation.py
### Script type: Python script
### Description: Printing Sum of Numbers in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
numbs=(2,3,4,5,6,7)
sum_nums=0
###============================================================================
### Main Functions
###============================================================================
for num in numbs:
    sum_nums=sum_nums+num
print(f'Sum of number is :{sum_nums}')
###============================================================================
### End
###============================================================================ 