###! /bin/python
###============================================================================
### Script name: Function_inside_function_exercise.py
### Script type: Python script
### Description: Basic Function inside function Exercise
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-04-27
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
def myfunc():
  x = 300
  print(x)

myfunc()
###============================================================================
### End
###============================================================================ 