###! /bin/python
###============================================================================
### Script name: Simple_Calculater.py
### Script type: Python script
### Description: Basic program to make a simple calculater
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Modules
###============================================================================
def add(p,q):
    return(p+q)
def substract(p,q):
    return(p-q)
def multiply(p,q):
    return(p*q)
def division(p,q):
    return(p/q)
###============================================================================
### Main Functions
###============================================================================
print("Please select the operation")
print("a.add")
print("b.substract")
print("c.multiply")
print("d.division")
choice=input("Please Enter Choice (a/b/c/d): ")
num_1=int(input("Enter the first number: "))
num_2=int(input("Enter the second number: "))

if choice=='a':
    print(num_1,"+",num_2,"=",add(num_1,num_2))
elif choice=='b':
    print(num_1,"-",num_2,"=",substract(num_1,num_2))
elif choice=='c':
    print(num_1,"*",num_2,"=",multiply(num_1,num_2))
elif choice=='d':
    print(num_1,"/",num_2,"=",division(num_1,num_2))
else:
    print("Invalid operation")
    print()
###============================================================================
### End
###============================================================================