###! /bin/python
###============================================================================
### Script name: Numpy_Exercise_2.py
### Script type: Python script
### Description: Numpy Join Two Arrays Exercise
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)k
### Version 3.0                                                Date: 2022-04-26
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Modules
###============================================================================
import pandas as pd
###============================================================================
### Main Functions
###============================================================================k

data = {
  "calories": [420, 380, 390],
  "duration": [50, 40, 45]
}

#load data into a DataFrame object:
df = pd.DataFrame(data)

print(df) 
###============================================================================
### End
###============================================================================ 

