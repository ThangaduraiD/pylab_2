###! /bin/python
###============================================================================
### Script name: Half_Diamond_printing.py
### Script type: Python script
### Description: Printing Half Diamond Pattern in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
rows = int(input("Enter Half Diamond Pattern Rows = "))
###============================================================================
### Main Functions
###============================================================================
for i in range(rows):
    for j in range(0, i + 1):
        print('* ', end = '')
    print()
for i in range(1, rows):
    for j in range(i, rows):
        print('* ', end = '')
    print()
###============================================================================
### End
###============================================================================ 