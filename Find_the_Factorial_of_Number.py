###! /bin/python
###============================================================================
### Script name: Find_the_factorial_of_Number.py
### Script type: Python script
### Description: Find the factorial of a given number
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
num=int(input("Enter Number: "))
factorial=1
###============================================================================
### Main Functions
###============================================================================
if num<0:
    print(" Factorial does not exist for negative numbers")  
elif num==0:
    print("factorial of 0 is 1")
else:
    for i in range(1,num+1):
        factorial=factorial*i
    print("The factorial of",num,"is",factorial)
###============================================================================
### End
###============================================================================