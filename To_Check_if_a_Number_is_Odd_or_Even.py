###! /bin/python
###============================================================================
### Script name: IF_ELSE_condition.py
### Script type: Python script
### Description: To Check if a Number is Odd or Even in Python by using IF Else
###              Condition
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
n=input("Enter Number: ")
n=int(n)
###============================================================================
### Main Functions
###============================================================================
if n%2==0:
    print("Given Number is Even")
else:
    print("Given number is odd")
  ###============================================================================
### End
###============================================================================

