
###! /bin/python
###============================================================================
### Script name: Quiz_Game.py
### Script type: Python script
### Description: Quiz game in Python
###
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-10
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
###============================================================================
### Main Functions
###============================================================================
print("Welcome to My Computer Quiz!")

playing=input("Do you want Play?: (yes/no)  ")

if playing.lower() == 'yes':
     print("Okay! Let's play: ) ")
     score=0
elif playing.lower() == 'no':
        quit()
###============================================================================       
# Question No: 1
###============================================================================
answer=input("What does IPS stand for?:  ")
if answer.lower()=="indian police service":
    print('Correct!')
    score += 1
else:
    print('Incorrect!')
###============================================================================
# Question No: 2
###============================================================================
answer=input("What does IAS stand for?:  ")
if answer.lower()=="indian administrative service":
    print('Correct!')
    score += 1
else:
    print('Incorrect!')
###============================================================================
# Question No: 3
###============================================================================
answer=input("What does IFS stand for?:  ")
if answer.lower()=="indian forest service":
    print('Correct!')
    score += 1
else:
    print('Incorrect!')
###============================================================================
# Question No: 4
###============================================================================
answer=input("What does RAM stand for?:  ")
if answer.lower()=="random access memory":
    print('Correct!')
    score += 1
else:
    print('Incorrect!')
###============================================================================
# Question No: 5
###============================================================================
answer=input("What does CPU stand for?:  ")
if answer.lower()=="central processing unit":
    print('Correct!')
    score += 1
else:
    print('Incorrect!')

print("Congratulations !")
print("You got " + str(score) + " Questions Correct!")
print("You got " + str((score/5) *100) +" % marks.")
###============================================================================
### End =======================================================================
###============================================================================
