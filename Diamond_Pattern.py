###! /bin/python
###============================================================================
### Script name: Diamond_pattern.py
### Script type: Python script
### Description: Printing Diamond Pattern in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
n=6
###============================================================================
### Main Functions
###============================================================================
for i in range(1,n+1):
    for j in range(1,n+1-i):
        print("",end=" ")
    for k in range(1,i+1):    
        print("*",end=" ")
    print()
for i in range(n-1,0,-1):
    for j in range(1,n+1-i):
        print("",end=" ")
    for k in range(1,i+1):    
        print("*",end=" ")
    print()
###============================================================================
### End
###============================================================================ 