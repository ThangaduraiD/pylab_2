###! /bin/python
###============================================================================
### Script name: Python_Scope_exercise.py
### Script type: Python script
### Description: Basic Python scope Exercise
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-04-27
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
def myfunc():
  x = 300
  def myinnerfunc():
    print(x)
  myinnerfunc()

myfunc()
###============================================================================
### End
###============================================================================ 