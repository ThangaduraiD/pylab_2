###! /bin/python
###============================================================================
### Script name: Step_parameter_method.py
### Script type: Python script
### Description: Basic Step parameter program in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
for x in range(1,20,5):
    print(x) #(1,1+5=6,6+5=11,11+5=16)
###============================================================================
### End
###============================================================================ 