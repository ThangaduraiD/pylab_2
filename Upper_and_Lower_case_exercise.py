###! /bin/python
###============================================================================
### Script name: Upper_and_Lower_case_exercise.py
### Script type: Python script
### Description: Simple program to find the upper & lower case of a given word
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
course="Python for beginners"
###============================================================================
### Main Functions
###============================================================================
print(course.upper())
print(course.lower())
print(course.find('for'))
print(course.replace('for','4'))
print(course.replace('o','5'))
print('Python' in course)
###============================================================================
### End
###============================================================================