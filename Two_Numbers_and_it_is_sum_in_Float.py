###! /bin/python
###============================================================================
### Script name: Two_Numbers_and_it_is_Sum_in_float.py
### Script type: Python script
### Description: Sum of Numbers in float 
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
num1 = input("Give some input:  ")
num2 = input("Give some input:  ")
###============================================================================
### Main Functions
###============================================================================
sum = float(num1)+float(num2)
print("Sum of {0} and {1} is {2}".format(num1,num2,sum))
###============================================================================
### End
###============================================================================ 
