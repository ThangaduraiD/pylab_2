###! /bin/python
###============================================================================
### Script name: Tuple_exercise.py
### Script type: Python script
### Description: Printing basic program in python by using tuple 
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
tup1=(1,2,3,4,5)
tup2=(6,7,8,9,0)
###============================================================================
### Main Functions
###============================================================================
print("tup1[1:5] : ",tup1[1:4])
###============================================================================
### End
###============================================================================ 