###! /bin/python
###============================================================================
### Script name: Json_Exercise.py
### Script type: Python script
### Description: json Exercise in Python
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-04-19
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
import json
###============================================================================
### Main Functions
###============================================================================
 
x = {
  "name": "John",
  "age": 30,
  "married": True,
  "divorced": False,
  "children": ("Ann","Billy"),
  "pets": None,
  "cars": [
    {"model": "BMW 230", "mpg": 27.5},
    {"model": "Ford Edge", "mpg": 24.1}
  ]
}

print(json.dumps(x))
###============================================================================
### End
###============================================================================ 





