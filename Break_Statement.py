###! /bin/python
###============================================================================
### Script name: Break_Statement.py
### Script type: Python script
### Description: Using Break Statement in Python
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
for i in range (1,7):
    if i%5==0:
        break
    print(i)
###============================================================================
### End
###============================================================================