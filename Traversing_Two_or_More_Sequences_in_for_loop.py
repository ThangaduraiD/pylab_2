###! /bin/python
###============================================================================
### Script name: traversing_two_or_more_sequences_in_for_loop.py
### Script type: Python script
### Description: Traversing Two or more Sequences in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
names = [ 'Alice', 'Bob', 'Trudy' ]
hobbies = [ 'painting', 'singing', 'hacking']
ages = [ 21, 17, 22 ]
###============================================================================
### Main Functions
###============================================================================
for person,age, hobby in zip(names,ages,hobbies):
       print (person+" is "+str(age)+"  years old and his/her hobby is "+hobby)
       print()
###============================================================================
### End
###============================================================================ 