###! /bin/python
###============================================================================
### Script name: Right_hand_side_half_triangle_printing.py
### Script type: Python script
### Description: Printing Right hand side half triangle in python by using for 
###              loop
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
for i in range(1,6):
    for j in range(1,i+1):
        print("*",end=" ")
    print()
###============================================================================
### End
###============================================================================