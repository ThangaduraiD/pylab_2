###! /bin/python
###============================================================================
### Script name: Functions_exercise.py
### Script type: Python script
### Description: Basic Functions Exercise
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-24
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
def result(a,b):
    sum=a+b
    sub=a-b
    mul=a*b
    div=a/b
    print(f"Sum is {sum},\nSub is {sub},\nMul is {mul},\nDiv is {div}")
a=int(input("Enter first Number: "))
b=int(input("Enter Second Number: "))
result(a,b)
###============================================================================
### End
###============================================================================ 