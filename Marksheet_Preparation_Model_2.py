
###! /bin/python
###============================================================================
### Script name: Marksheet_Preparation_Model_2.py
### Script type: Python script
### Description:Creating JSON Array in Python
###
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-21
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Modules
###============================================================================
import json
###============================================================================
###Local Variables
###============================================================================
f=open("Students.json")
data=json.load(f)
###============================================================================
###Main Functions
###============================================================================
# CONVERT USER STRING TO JSON
data = str(data)
print(type(data))
lst = []
u_in = 0
while True:
    u_in = u_in+1
    print('Enter Input ', u_in, ' : ')
    val = input("")
    lst.append(val)
    if u_in == 3:
        break
jsonlist = [{'data1': lst[0]}, {'data2': lst[1]}, {'data3': lst[2]}]
jsonString = json.dumps(jsonlist, indent=4)
print(jsonString)

with open("myjson.json", "w") as outfile: 
    json.dump(json.loads(jsonString), outfile) 
###============================================================================
###End
###============================================================================