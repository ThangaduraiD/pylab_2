###! /bin/python
###============================================================================
### Script name: Functions_exercise_in Parameter_using.py
### Script type: Python script
### Description: Functions Parameter in python
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-04-01
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
answer = 10

def multiply(num1 , num2):
    answer = num1 * num2
    print ("answer inside : ",answer)
    return answer

multiply(10 , 2)
print ("answer outside : ",answer)
###============================================================================
### End
###============================================================================ 