###! /bin/python
###============================================================================
### Script name: IF_ELSE_condition_model_2.py
### Script type: Python script
### Description: To Check if a Number is Low or Medium or Large in Python by 
###              using IF Else Condition
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
n=input("Enter Any Number: ")  
n=int(n) 
###============================================================================
### Main Functions
###============================================================================
if n>=1 and n<=10:
    print("too low")
elif n>=11 and n<=20:
    print("medium")   
elif n>=21 and n<=30:
    print("large")
else:
    print("too large")
###============================================================================
### End
###============================================================================
 

