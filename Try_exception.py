###! /bin/python
###============================================================================
### Script name: Try_exception.py
### Script type: Python script
### Description: Try exception in Python
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-04-01
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
x = 10
if x > 5:
    raise Exception('x should not exceed 5. The value of x was: {}'.format(x))
###============================================================================
### End
###============================================================================
