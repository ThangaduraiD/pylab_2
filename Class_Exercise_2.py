###! /bin/python
###============================================================================
### Script name: Class_Exercise.py
### Script type: Python script
### Description: Class Exercise in Python
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-04-19
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
class Person:
###============================================================================
### Main Functions
###============================================================================

  def __init__(self, name, age):
    self.name = name
    self.age = age


  def myfunc(self):
    print("Hello my name is " + self.name)


p1 = Person("John", 36)
p1.myfunc()
###============================================================================
### End
###============================================================================ 

