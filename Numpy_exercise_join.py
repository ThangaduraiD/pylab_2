###! /bin/python
###============================================================================
### Script name: Numpy_Exercise_2.py
### Script type: Python script
### Description: Numpy Join Two Arrays Exercise
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)k
### Version 3.0                                                Date: 2022-04-26
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Modules
###============================================================================
import numpy as np
###============================================================================
### Main Functions
###============================================================================k
arr1 = np.array([1, 2, 3])
arr2 = np.array([4, 5, 6])

arr = np.concatenate((arr1, arr2))

print(arr)
###============================================================================
### End
###============================================================================ 