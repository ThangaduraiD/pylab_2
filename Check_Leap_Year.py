###! /bin/python
###============================================================================
### Script name: Check_leap_year_or_not.py
### Script type: Python script
### Description: Checking,given year leap year or not
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Modules
###============================================================================
def checkLeap(year):
###============================================================================
### Main Functions
###============================================================================
    if ((year%400==0) or
       (year%100!=0)and
        (year%4==0)):
        print("Given Year is Leap Year.")
    else:
        print("Given year is not a Leap Year.") 
year=int(input("Enter Any year: "))   
checkLeap(year)
###============================================================================
### End
###============================================================================