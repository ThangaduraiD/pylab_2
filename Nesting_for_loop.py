###! /bin/python
###============================================================================
### Script name: Nesting_For_loop.py
### Script type: Python script
### Description: Printing each letter of a given word in python by using 
###              Nesting for loop
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
words=["Book","Note","pen"]
###============================================================================
### Main Functions
###============================================================================
for word in words:
    print("The following lines will print each letters of "+word)
    for letter in word:
        print(letter)
###============================================================================
### End
###============================================================================ 