###! /bin/python
###============================================================================
### Script name: Each_letter_print_in_for_loop.py
### Script type: Python script
### Description: Printing Each letter in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Local Variables
###============================================================================
word=input("Enter any Name: ")
###============================================================================
### Main Functions
###============================================================================
for letter in word:
    print(letter)
###============================================================================
### End
###============================================================================