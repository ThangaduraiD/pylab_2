###! /bin/python
###============================================================================
### Script name: Range_function_method.py
### Script type: Python script
### Description: Basic range function program in python by using for loop
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Main Functions
###============================================================================
for x in range(4):
    print(x)
###============================================================================
### End
###============================================================================ 