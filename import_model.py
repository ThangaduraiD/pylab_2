###! /bin/python
###============================================================================
### Script name: Import_model.py
### Script type: Python script
### Description: Basic program in python by using Import modules
###              
###
### Developed by Thangadurai D (dthangadurai2486@gmail.com)
### Version 3.0                                                Date: 2022-03-17
### Reviewed by                    Date:
### Approved by                    Date:
###============================================================================
### Modules
###============================================================================
import math
from secrets import choice
###============================================================================
### Local Variables
###============================================================================
number=16
###============================================================================
### Main Functions
###============================================================================
number=math.sqrt(number)
print(number)
###============================================================================
### End
###============================================================================ 